Rails.application.routes.draw do
  resources :stories
  devise_for :users
  root to: 'words#new'

  resources :word_types
  get 'words/rate' => 'words#rate', as: 'rate_word'
  delete 'words/destroy_all' => 'words#destroy_all', as: 'destroy_all_words'
  get 'words/:id/rate' => 'words#save_rating', as: 'save_word_rating'

  resources :words
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
