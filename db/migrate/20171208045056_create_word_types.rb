class CreateWordTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :word_types do |t|
      t.string :key, unique: true
      t.string :name, unique: true
      t.text :description
      t.timestamps
    end
  end
end
