class CreateWordRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :word_ratings do |t|
      t.integer :word_id, null: false
      t.integer :user_id, null: false
      t.integer :rating, null: false

      t.timestamps
    end
  end
end
