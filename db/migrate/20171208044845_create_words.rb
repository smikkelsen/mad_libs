class CreateWords < ActiveRecord::Migration[5.1]
  def change
    create_table :words do |t|
      t.string :value, null: false
      t.integer :user_id, null: false
      t.integer :down_votes, default: 0, null: false
      t.integer :up_votes, default: 0, null: false
      t.integer :word_type_id, index: true, null: false

      t.timestamps
    end
  end
end
