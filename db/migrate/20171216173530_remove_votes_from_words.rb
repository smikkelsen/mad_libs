class RemoveVotesFromWords < ActiveRecord::Migration[5.1]
  def change
    remove_column :words, :down_votes
    remove_column :words, :up_votes
  end
end
