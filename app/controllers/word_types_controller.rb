class WordTypesController < ApplicationController
  before_action :set_word_type, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  def index
    @word_types = WordType.all
  end

  def show
  end

  def new
    @word_type = WordType.new
  end

  def edit
  end

  def create
    @word_type = WordType.new(word_type_params)

    if @word_type.save
      redirect_to word_types_url, notice: 'Word type was successfully created.'
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      if @word_type.update(word_type_params)
        format.html {redirect_to @word_type, notice: 'Word type was successfully updated.'}
        format.json {render :show, status: :ok, location: @word_type}
      else
        format.html {render :edit}
        format.json {render json: @word_type.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @word_type.destroy
    respond_to do |format|
      format.html {redirect_to word_types_url, notice: 'Word type was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  def set_word_type
    @word_type = WordType.find(params[:id])
  end

  def word_type_params
    params.require(:word_type).permit(:key, :name, :description)
  end
end
