class WordsController < ApplicationController
  before_action :set_word, only: [:show, :edit, :update, :destroy, :save_rating]
  load_and_authorize_resource

  def index
    @words = Word.accessible_by(current_ability).order(created_at: :desc)
  end

  def show
  end

  def rate
    voted_for = WordRating.where(user_id: current_user.id).pluck(:word_id)
    @word = Word.joins('LEFT JOIN word_ratings ON word_ratings.word_id = words.id').
        where.not(user_id: current_user.id).
        where.not(words: {id: voted_for}).
        select('words.*, COUNT(word_ratings.id)').group('words.id').
        order('count ASC').limit(6).shuffle.first
    @word_type = @word.word_type if @word
  end

  def save_rating
    WordRating.create(user_id: current_user.id, word_id: @word.id, rating: params[:rating])
    redirect_to rate_word_path
  end

  def new
    @word = Word.new
    @word_type = WordType.choose_needed
    @word.word_type_id = @word_type&.id
  end

  def edit
  end

  def create
    @word = Word.new(word_params)
    @word.user_id = current_user.id

    unless @word.save
      flash[:danger] = @word.errors.full_messages.join(' ')
    end
    redirect_to new_word_path
  end

  def update
    respond_to do |format|
      if @word.update(word_params)
        format.html {redirect_to @word, notice: 'Word was successfully updated.'}
        format.json {render :show, status: :ok, location: @word}
      else
        format.html {render :edit}
        format.json {render json: @word.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @word.destroy
    respond_to do |format|
      format.html {redirect_to words_url, notice: 'Word was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  def destroy_all
    Word.destroy_all
    redirect_to words_url, notice: 'Words were successfully destroyed.'
  end

  private
  def set_word
    @word = Word.find(params[:id])
  end

  def word_params
    params.require(:word).permit(:value, :word_type_id)
  end
end
