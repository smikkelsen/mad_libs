class User < ApplicationRecord
  has_many :words, dependent: :nullify
  has_many :word_ratings, dependent: :nullify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def admin?
    self.admin
  end
end
