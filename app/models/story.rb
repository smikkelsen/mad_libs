class Story < ApplicationRecord

  def body_with_words
    if @body_with_words.nil?
      @body_with_words = self.body.clone
      matches = @body_with_words.scan(/\{!(.+?)\}/) unless self.body.nil?

      # Check matches against built in variables
      matches&.each do |match|
        match = match.first
        if match.include?(':')
          splits = match.split(':')
          global = splits[1]
          match = splits[0]
        end

        word_type = WordType.where(key: match).first
        word = word_type&.get_random_word
        next if word.nil?
        value = word&.value
        value ||= ''
        value = add_word_data(word_type, word, value)
        if global.present?
          @body_with_words.gsub!(/{!#{match}:#{global}}/i, value)
        else
          @body_with_words.sub!(/{!#{match}}/i, value)
        end
      end
    end
    @body_with_words
  end

  private

  def add_word_data(word_type, word, value)
    text = "word type: #{CGI::escapeHTML(word_type.name)}<br>user: #{CGI::escapeHTML(word.user&.email)}"
    value = "<span class='replacement toolpop' data-toggle='popover' data-trigger='click' title='Word Info' data-content=\"#{text}\">#{value}</span>" unless value.blank?
    value
  end
end
