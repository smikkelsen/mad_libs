class WordRating < ApplicationRecord
  belongs_to :user
  belongs_to :word

  after_commit :check_word
  validates_uniqueness_of :user, scope: :word
  validates_presence_of :user_id, :word_id, :rating

  private
  def check_word
    self.word.check_ratings
  end
end
