class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    else
      can [:create, :rate, :save_rating, :read], Word
      can [:read], Story
    end
  end

end