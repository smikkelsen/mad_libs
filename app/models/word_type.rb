class WordType < ApplicationRecord
  has_many :words, dependent: :delete_all

  validates :key, uniqueness: {case_sensitive: false}
  validates :name, uniqueness: {case_sensitive: false}

  def get_random_word
    Word.uncached do
      self.words.order("RANDOM()").first
    end
  end

  def self.choose_needed
    word_types = self.all.joins('LEFT JOIN words on words.word_type_id = word_types.id').
        select('word_types.*, COUNT(words.id)').
        group('word_types.id').
        order('count ASC').limit(3)
    word_types.shuffle.first
  end
end
