class Word < ApplicationRecord
  belongs_to :word_type
  belongs_to :user
  has_many :word_ratings, dependent: :delete_all

  DOWNVOTE_THRESHOLD = 2

  validates_presence_of :value, :user_id, :word_type_id
  validates :value, uniqueness: {case_sensitive: false, scope: :word_type_id}

  scope :down_votes, -> (word_id) {joins(:word_ratings).where(word_ratings: {word_id: word_id, rating: -1})}
  scope :up_votes, -> (word_id) {joins(:word_ratings).where(word_ratings: {word_id: word_id, rating: 1})}
  scope :skips, -> (word_id) {joins(:word_ratings).where(word_ratings: {word_id: word_id, rating: 0})}

  def check_ratings
    if self.word_ratings.where(rating: -1).count >= DOWNVOTE_THRESHOLD
      self.destroy
    end
  end

  def down_votes
    self.class.down_votes(self.id)
  end

  def up_votes
    self.class.up_votes(self.id)
  end

  def skips
    self.class.skips(self.id)
  end
end
